package com.sunnyelectra

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.sunnyelectra.databinding.FragmentRawBeanBinding
import timber.log.Timber

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

/**
 * A simple [Fragment] subclass.
 * Use the [RawBeanFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RawBeanFragment : Fragment() {
    private lateinit var recyclerViewAdapter: MainRecyclerViewAdapter
    private lateinit var viewModel: MainViewModel
    private lateinit var binding: FragmentRawBeanBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_raw_bean, container, false)
        viewModel = MainViewModel()
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerViewAdapter()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment RawBeanFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            RawBeanFragment()
    }

    private fun setupRecyclerViewAdapter() {
        val viewModel = binding.viewModel
        if (viewModel != null) {
            recyclerViewAdapter = MainRecyclerViewAdapter(viewModel)
            binding.mainRecycler.apply {
                adapter = recyclerViewAdapter
                layoutManager = LinearLayoutManager(requireContext())
            }
        } else {
            Timber.w("ViewModel not initialized when attempting to set up adapter.")
        }
    }
}