package com.sunnyelectra

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.sunnyelectra.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =
            DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        showRawBeans()
    }

    private fun showRawBeans() {
        supportFragmentManager.beginTransaction()
            .add(binding.hostFragment.id, RawBeanFragment.newInstance())
            .commit()
    }
}