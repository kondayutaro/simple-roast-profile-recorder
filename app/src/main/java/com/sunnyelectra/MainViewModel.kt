package com.sunnyelectra

import androidx.lifecycle.ViewModel
import com.jakewharton.rxrelay3.BehaviorRelay
import com.sunnyelectra.data.RawBean


class MainViewModel constructor() : ViewModel() {
    val items: BehaviorRelay<List<RawBean>> = BehaviorRelay.createDefault(
        listOf(
            RawBean(
                "Doi Chaang", "Thailand", "Make Brown", "20210618", 1
            ),
            RawBean(
                "Sidamo Shakisso", "Ethiopia", "Make Brown", "20210618", 1
            )
        )
    )
}