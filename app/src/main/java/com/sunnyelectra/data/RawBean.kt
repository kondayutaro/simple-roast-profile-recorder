package com.sunnyelectra.data

data class RawBean constructor(
    val name: String,
    val originCountry: String,
    val seller: String,
    val lastUpdatedAt: String,
    val numberOfProfiles: Int
)