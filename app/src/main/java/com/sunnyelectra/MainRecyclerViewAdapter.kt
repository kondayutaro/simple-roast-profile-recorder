package com.sunnyelectra

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sunnyelectra.data.RawBean
import com.sunnyelectra.databinding.CellMainRecyclerBinding

class MainRecyclerViewAdapter constructor(
    private val viewModel: MainViewModel
) :
    RecyclerView.Adapter<MainRecyclerViewAdapter.MainRecyclerViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainRecyclerViewHolder =
        MainRecyclerViewHolder.from(parent)

    override fun onBindViewHolder(holder: MainRecyclerViewHolder, position: Int) {
        holder.bind(viewModel, viewModel.items.value[position])
    }

    override fun getItemCount(): Int = viewModel.items.value.size

    class MainRecyclerViewHolder private constructor(private val binding: CellMainRecyclerBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(viewModel: MainViewModel, item: RawBean) {
            binding.viewModel = viewModel
            binding.item = item
        }

        companion object {
            fun from(parent: ViewGroup): MainRecyclerViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = CellMainRecyclerBinding.inflate(layoutInflater, parent, false)

                return MainRecyclerViewHolder(binding)
            }
        }
    }
}

